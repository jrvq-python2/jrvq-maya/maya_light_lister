# -*- coding: UTF-8 -*-
"""
Author: Jaime Rivera
File: maya_specifics.py
Date: 2019.06.27
Revision: 2020.01.12
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief:

"""

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'


from PySide2 import QtWidgets
from maya import cmds


# -------------------------------- INFO COLLECTION -------------------------------- #
def get_all_lights():
    all_lights = [node for node in cmds.ls(type=["light"] + cmds.listNodeTypes("light"))]
    return all_lights

def get_node_type(node):
    return cmds.nodeType(node)

def get_the_transform(shape):
    parent = cmds.listRelatives(shape, parent=True)
    if parent:
        return parent[0]

    return 'No transform'

def get_an_attribute(object, attr):
    if cmds.objExists(object + '.' + attr):
        return cmds.getAttr(object + '.' + attr)
    else:
        return None

def check_for_connections(shape, attr):
    connected = cmds.connectionInfo(shape + '.' + attr, id=True)
    locked = cmds.getAttr(shape + '.' + attr, lock=True)

    return connected or locked

# -------------------------------- MANIPULATION -------------------------------- #
def select_something(object):
    cmds.select(object)

def rename_something(old_name, new_name):
    try:
        cmds.rename(old_name, new_name)
    except RuntimeError as e:
        error_msg = '<b><font size=4>RuntimeError:<br>' + str(e)
        QtWidgets.QMessageBox.warning(None, 'Rename error', error_msg, QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.NoButton)
        raise

def set_color_attr(shape, new_color):
    cmds.setAttr(shape + '.color', new_color.redF(), new_color.greenF(), new_color.blueF())

def set_attr(obj, attr, new_number):
    cmds.setAttr(obj + '.' + attr, new_number)
